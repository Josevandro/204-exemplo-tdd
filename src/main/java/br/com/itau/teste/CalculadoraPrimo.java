package br.com.itau.teste;

public class CalculadoraPrimo {
	public boolean ePrimo(int numero) {
		if(numero <= 1) {
			throw new NaoAplicavelException();
		}
		
		for(int i = 2; i < numero; i++) {
			if(eDivisivel(numero, i)) {
				return false;
			}
		}
		
		return true;
	}
	
	private boolean eDivisivel(int a, int b) {
		return a % b == 0;
	}
}
