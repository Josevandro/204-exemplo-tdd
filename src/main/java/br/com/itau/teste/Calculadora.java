package br.com.itau.teste;

public class Calculadora {

	public int somar(int a, int b) {
		return a + b;
	}
	
	public int subtrair(int a, int b) {
		return a - b;
	}
	
	public int dividir(int a, int b) {
		return a / b;
	}
	
	public int retornarMaior(int a, int b) {
		if(a > b) {
			return a;
		}
		
		return b;
	}
}
