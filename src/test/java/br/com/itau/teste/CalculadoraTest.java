package br.com.itau.teste;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class CalculadoraTest {
	private Calculadora calculadora;
	
	@Before
	public void preparar() {
		calculadora = new Calculadora();
	}
	
	@Test
	public void deveSomarDoisNumeros() {
		//given | dado que | setup
		//Calculadora calculadora = new Calculadora();
		
		//when | quando | check
		int resultado = calculadora.somar(1, 1);
		
		//then | então | action
		assertEquals(2, resultado);
	}
	
	@Test
	public void deveSubtrairDoisNumeros() {
		int resultado = calculadora.subtrair(7, 3);
		
		assertEquals(4, resultado);
	}
	
	@Test
	public void deveDividirUmNumero() {
		int resultado = calculadora.dividir(10, 2);
		
		assertEquals(5, resultado);
	}
	
	@Test(expected = ArithmeticException.class)
	public void deveDividirPorZero() {
		calculadora.dividir(10, 0);
	}
	
	@Test
	public void deveRetonarOMaiorEntreDoisNumeros() {
		int resultadoA = calculadora.retornarMaior(10, 1);
		int resultadoB = calculadora.retornarMaior(1, 10);
		
		assertEquals(10, resultadoA);
		assertEquals(10, resultadoB);
	}
}
