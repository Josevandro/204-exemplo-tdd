package br.com.itau.teste;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

public class CalculadoraPrimoTest {

	private CalculadoraPrimo calculadora;
	
	@Before
	public void preparar() {
		calculadora = new CalculadoraPrimo();
	}
	
	@Test
	public void deveDeterminarSeUmNumeroEPrimo() {
		assertTrue(calculadora.ePrimo(2));
		assertTrue(calculadora.ePrimo(3));
		assertTrue(calculadora.ePrimo(11));
		assertFalse(calculadora.ePrimo(15));
		assertFalse(calculadora.ePrimo(10));
		assertFalse(calculadora.ePrimo(18));
	}
	
	@Test(expected = NaoAplicavelException.class)
	public void deveLancarExcessaoParaNumerosAbaixoDe2() {
		calculadora.ePrimo(0);
	}
}
